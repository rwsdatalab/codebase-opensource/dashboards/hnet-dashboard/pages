Privacy
##########################

Privacy is een zeer belangrijk speerpunt in ieder project. Met het **HNet-dashboard** wordt de data daarom ook niet **niet** op een *server* of in een *cloud* omgeving opgeslagen. De data blijft lokaal en de berekening worden ook lokaal uitgevoerd.


Demo dataset
##########################

De Titanic demo data set kan `hier gedownload worden <https://rwsdatalab.gitlab.io/codebase-opensource/dashboards/hnet/example_datasets\titanic.csv>`_.


Artikel
##########################

Voor meer details over HNet:
	* `Artikel op Arxiv <https://arxiv.org/abs/2005.04679>`_
	* `Blog op Medium  <https://towardsdatascience.com/explore-and-understand-your-data-with-a-network-of-significant-associations-9a03cf79d254>`_


Stand-alone
##########################

HNet kan ook gebruikt worden in een stand-alone versie met Python.
	* `Github pagina <https://github.com/erdogant/hnet>`_
