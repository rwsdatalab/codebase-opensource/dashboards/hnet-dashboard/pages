
Met het **HNet-dashboard** wordt het mogelijk gemaakt om zelf datasets te analyseren.
Om te voorkomen dat variabelen worden meegenomen die **apriori** al niet interresant zijn, zoals bijvoorbeeld identificatie nummers, kunnen er selecties en filters worden toegepast alvorens het analyseren.


Input data
################

De dataset kan ge-analyseerd worden door het data-bestand in het blauw gestreepte rechthoek te slepen of door op "Select a file or drag it here" te klikken. Ter illustratie zal de bekende **Titanic** dataset ge-analyseerd worden dat
`hier gedownload kan worden <https://rwsdatalab.gitlab.io/codebase-opensource/dashboards/hnet/example_datasets\titanic.csv>`_.
De ongestructeerde dataset heeft als eerste rij de variabelen namen en de variabelen zijn *komma* "," gescheiden.

.. |figR1| image:: ./figs/screenshot.png
.. |figR2| image:: ./figs/raw_titanic_dataset.png

.. table::  Ongestructureerde input dataset voor het dashboard.
   :align: center

   +----------+----------+
   | |figR1|  | |figR2|  |
   +----------+----------+


Na het opgegeven van de dataset zal de data ingelezen en op een gestructureerde manier getoond worden (zie hieronder).
Iedere rij toont nu een **variable** naam met daarachter de ingelezen content. De waarden die meest frequent aanwezig zijn, worden als eerste getoond en het aantal van de betreffende waarde wordt aangegeven tussen de haakjes **( x )**. Naast de variabele naam is ook een kolom **Type**, dat beschrijft of de *content* van het type **categorie** of **numeriek** is. Dit kan handmatig omgezet worden indien nodig.


.. |figR3| image:: ./figs/screenshot_titanic.png

.. table:: Dashboard na het verwerken van de data.
   :align: center

   +----------+
   | |figR3|  |
   +----------+



.. warning::
	Het is belangrijk om een weloverwogen keuze te maken in de **Type**: *categorie (cat)* of *numeriek (num)*. **Niet** iedere categorische waarde is te beschrijven als een **numeriek** waarde en andersom. Bijvoorbeeld, de kolomnaam **Survived** bevat de waarde 0 en 1 en beschrijft of een passengier het overleeft heeft. Ondanks dat het weergegeven is als een numerieke waarde, wordt dit niet persee als zodanig bedoeld. Op het moment dat het als numerieke waarde wordt beschouwd, dan zal het model een waarde tussen 0 en 1 gaan zoeken om de "overleving" te gaan beschrijven.


Selectie op Variabelen
########################

Een **selectie** op variabelen is een belangrijke stap om de **complexiteit te verlagen** en de rekentijd te verminderen. In dit voorbeeld zijn de *identifiers* en *namen* ge-excludeerd. Er kunnen ook nog domein specifieke keuzens gemaakt kunnen worden om niet interresante variabelen uit het model te verwijderen, zoals bijvoorbeeld *Ticket* en/of *Cabin*. Alleen als de **Checkbox** aangevinkt is, dan wordt variabele meegenomen in de analyse. Naast een *selectie* is het ook mogelijk om individuele waarden te verwijderen uit het model. Dit kan met de **Filtering** stap gedaan worden (zie sectie *Filtering*).


.. |figR4| image:: ./figs/screenshot_titanic_selection.png

.. table:: Selectie van variabelen aan de hand van de checkbox.
   :align: center

   +----------+
   | |figR4|  |
   +----------+


Filteren op Waarden
##########################

Een **Filtering** op variabelen heeft, net als *selectie*, ook de funtie om de **complexiteit** en de **rekentijd** te verminderen. In tegenstelling tot de selectie, kan je bij de filtering fijnmaziger keuzes maken. Als voorbeeld, stel je wilt de waarde **0** niet meenemen voor de variabele **SidSp**, dan kan dit simpelweg ge-excludeerd worden door op de betreffende waarde te klikken.

Anderzijds is het ook mogelijk om variabelen te verwijderen die niet voldoende voorkomen. Dit kan gedaan worden in in de grijze box **"Only include categories that occur at least n times and at least k percent of all featre values."**


.. |figR5| image:: ./figs/screenshot_titanic_filter.png

.. table:: Filtering van variabelen.
   :align: center

   +----------+
   | |figR5|  |
   +----------+


Missende waarden
############################

Het ``HNet`` model kan omgaan met missende waarden. Dit betekent dat een variabele af-en-toe een waarde mag missen. Missende waarden worden doorgaans aangegeven met **Not Any Number (NaN)** of **None** of een **spatie** maar een andere (persoonlijke) vorm kan ook. De missende waarden kunnen aangegeven worden in het grijze box **Missing values**. Hiermee worden de missende waarden door de gehele dataset aangeduid voor het model.


Starten van de analyse
############################

Nadat alle selecties en filters uitgevoerd zijn kunnen de analyse uitgevoerd worden door op de **Explore associations** knop te klikken.

.. tip::
	Het **HNet-dashboard** werkt in een *web-browser* om de tooling zo onafhankelijk mogelijk te houden van de verschillende type systemen. Het werken in een webbrowser kan ook nadelen hebben zoals dat er een default time-out periode van ongeveer 5 minuten aanwezig is. Dat wil zeggen dat de website inactief wordt als er in dit tijdvak niet op de knop **Explore associations** gedrukt is.
