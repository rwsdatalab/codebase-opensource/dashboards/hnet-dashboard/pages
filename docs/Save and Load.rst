
Het opslaan en laden van resultaten kan gewenst zijn als om op een later moment de data verder te verkennen zonder opnieuw alle selecties en filtering stappen uit te voeren. Daarnaast kan het voorkomen dat de detectie van associates tientallen minuten in beslag kan nemen, afhankelijk van de grootte van de dataset.


Opslaan
----------------

Opslaan van de resultaten kan gedaan worden door op de "opslaan" icoon te klikken.

.. |figS1| image:: ./figs/hnet_explore_3.png

.. table:: Resultaten HNet.
   :align: center

   +----------+
   | |figS1|  |
   +----------+




Laden
----------------------

Het laden van een opgeslagen model kan eenvoudig gedaan worden door de file te selecteren of slepen in het "Select a file or drag it here".


.. |figS2| image:: ./figs/screenshot.png

.. table::  Laden model.
   :align: center

   +----------+
   | |figS2|  |
   +----------+
