
De resultaten van het **HNet-dashboard** worden getoond in een **netwerk diagram** en een **heatmap** die ook aan elkaar gekoppeld zijn. Een selectie in de een grafiek zal ook dezelfde selectie bij de andere grafiek maken.

Netwerk
##########

Met het interactieve netwerk zijn **alleen** de significante associaties tussen de variabelen getoont. Met deze visualisatie kan het netwerk, en dus de complexe relaties tussen variabelen, dieper onderzocht worden. Een belangrijk hulpmiddel is de schuifregelaar waarmee geleidelijk de relaties "gebroken" kunnen worden op significatie. De slider is gebaseerd op de *-log10(P-waarde)* met een (standaard) minimum *-log10(0.05)*. Door de schuifregelaars naar de rechts te schuiven zullen alleen de meest significante relaties behouden blijven. Verder heeft iedere *node* een label dat de **kolom naam** + **variable naam** is. De relaties tussen de nodes kunnen worden gemarkeerd wanneer een dubbel klik op de node gegeven wordt.

Voordelen netwerk diagram:
	* Dubbelklik op een node toont alleen de relaties die eraan verbonden zijn.
	* Met het muiswiel kan men inzoomen in de grafiek.
	* Door de schuifregelaar naar rechts te bewegen behoudt men alleen de meest significante relaties.

Heatmap
##########

Een heatmap is ideaal wanneer het netwerk een gigantische haarbal wordt. De effectiviteit van een matrix-diagram is sterk afhankelijk van de volgorde van rijen en kolommen: als gerelateerde *nodes* dicht bij elkaar worden geplaatst, is het gemakkelijker om *clusters* te identificeren. Hoewel het volgen van een pad moeilijker is in een matrixweergave dan in een netwerk, hebben matrices andere voordelen. Zo kunnen de cellen ook worden gekleurd op clusters op basis van een community-detectie-algoritme.


.. |figO1| image:: ./figs/hnet_explore_1.png

.. table:: Resultaten HNet.
   :align: center

   +----------+
   | |figO1|  |
   +----------+


.. |figO2| image:: ./figs/hnet_explore_2.png

.. table:: Resultaten nadat de slider bovenin meer naar rechts verschoven is. Wat we zien is dat er twee sterkere clusters overblijven, namelijk een netwerk met "female" en "Survived=1", en los het netwerk van "Male" en "Survived=0".
   :align: center

   +----------+
   | |figO2|  |
   +----------+
