image: ubuntu:22.04

# define global variables
variables:
  LC_ALL: "en_US.UTF-8"
  LANG: "en_US.UTF-8"
  DEBIAN_FRONTEND: "noninteractive"

# define aliases
.setup_build_environment: &setup_build_environment
  - apt-get update -qq && apt-get install -y -qq locales git && locale-gen en_US.UTF-8
  - apt-get install -y -qq python3-venv python3-dev build-essential
  - python3 -m venv /build_venv && source /build_venv/bin/activate
  - python3 -m pip install --upgrade pip
  - python3 -m pip install wheel

.install_dev_requirements: &install_dev_requirements
  - python3 -m pip install -r requirements-doc.txt
  - python3 -m pip install anybadge

.install_doc_requirements: &install_doc_requirements
  - apt-get update -qq && apt-get install -y -qq fonts-freefont-otf latexmk texlive-fonts-recommended texlive-latex-recommended texlive-latex-extra texlive-xetex xindy

.setup_default_doc_badge: &setup_default_doc_badge
  - anybadge --label=docs --value=failed --file=documentation.svg passed=green failed=red

.overwrite_doc_badge: &overwrite_doc_badge
  - anybadge --overwrite --label=docs --value=passed --file=documentation.svg passed=green failed=red

.install_static_analysis_requirements: &install_static_analysis_requirements
  - python3 -m pip install -U restructuredtext-lint

.build_all_documentation: &build_all_documentation
  - sphinx-build -W --keep-going -n -b html docs docs/_build/html
  - cp -r docs/_build/html public
  - (cd docs && make latexpdf)
  - (cp docs/_build/latex/pages.pdf public)

# default before_script if none is defined (used for static_analysis stage)
before_script:
  - *setup_build_environment
  - *install_dev_requirements

# define ci/cd stages
stages:
  - static analysis
  - test
  - deploy

# static analysis stage
rst_lint:
  stage: static analysis
  before_script:
    - *setup_build_environment
    - *install_dev_requirements
    - *install_static_analysis_requirements
  script:
    - restructuredtext-lint README.rst

# test stage
documentation:
  stage: test
  rules:
    - if: $CI_COMMIT_BRANCH && $CI_COMMIT_BRANCH != "main"
  before_script:
    - *setup_build_environment
    - *install_dev_requirements
    - *install_doc_requirements
  script:
    - *build_all_documentation

# deploy stage
pages:
  stage: deploy
  rules:
    - if: $CI_COMMIT_BRANCH == "main"
  before_script:
    - *setup_build_environment
    - *install_dev_requirements
    - *install_doc_requirements
    - *setup_default_doc_badge
  script:
    - *build_all_documentation
    - *overwrite_doc_badge
  artifacts:
    paths:
    - documentation.svg
    - public
